# Automated Monitoring System For Cyber Range

## Technologies:
- Python3
- Zabbix monitoring solution.
- Ansible.
- Terraform.


This projects contains all the steps to have a Zabbix server combined with ssh tunnels allowing the monitoring of a cyber range by an external person.

The creation of a zabbix machine is done by deploying a Debian 11 virtual machine on Microsoft azure using Terraform.

The creation of tunnels is done in python3.
We have provided an Ansible playbook allowing the installation of the requirements and the tunnels creation on a remote machine.

Finally, we push the monitoring task using python3 `pyzabbix` librarie to communicate with Zabbix API.

## Files to modify

The terraform variables : `terraform_scripts\example.tfvars`

The .yml file containing the tunnels, the hosts informations and the monitoring tasks : `python_scripts\data.yml`

And the ansible inventory if you want to run the tunnel creation script on a remote machine : `ansible\inventory.yml`




# Description
This project is split in 3 main steps :

1. Zabbix server deployment through Terraform script on Azure provider.
2. SSH tunnels creation on the Zabbix server using python librarie.
3. Adding the monitoring tasks on the Zabbix server using its API with python "pyzabbix" librarie.


# How to run the scripts

## Terraform Deployment

All the needed scripts and information to deploy a Debian 11 vm which is provisioned in a Zabbix server are in the *terraform_scripts* folder.


## SSH tunnels creation

### Requirements

The tunnels creation script has been tested on Debian 11 with Python3 pip and several python 3 packages : sshtunnel, pytunneling, pyyaml.


- `apt install python3-pip`
- `pip install sshtunnel`
- `pip install pytunneling`
- `pip install pyyaml`

### How does the script work ?


The 2 last steps of the project (SSh tunnels creation and Zabbix tasks creation) use the same variable file as input.
The file is a .yml file with the following informations :

- Each devices ip address, ssh user name and ssh password.
- The tunnels which are composed of :
	- A name
	- A path  ( A => B => C means that the machine on which the script runs will have a tunnel connecting to machine A, then Machine B and ending on machine C)
	- The interface on which to add the tunnel (0.0.0.0 to allow a connection from any interface)
	- The local port on which will be the tunnel entry.
- The List of monitoring tasks containing:
	- The host to monitor.
	- The tunnel or port we have to use to reach their ssh server.
	- The monitoring tasks.

	An example of the parameter file is available at `python_scripts\data_example.yml`

### How to run the script.

#### Running on the local machine:

simply run the python script `python3 python_scripts/tunnel_creation.py`. You will see the output for every created tunnel.

#### Running on remote server.

To create the tunnels on a remote server (The zabbix server in our case):
1. Create your ansible inventory file according to `ansible_scripts/inventory_example.yml`
2. run the ansible playbook `ansible_scripts/create_ssh_tunnel_on_remote_server.yml -i ansible_scripts/inventory.yml`.

The ansible playbook ensures that the server has the needed packages, and then run the tunnels in background.


## Zabbix monitoring task creation.

### How does this step work ?

For this step, we take the same variables file as the SSH tunnels creation step. As explained in the SSH tunnel creation step.

We need one file for the 2 last steps.
This allows the user to abstract the tunnels.

Indeed, the script take the parameter files, and modify each target address by the corresponding tunnel when needed.

When this step is done, we push the Zabbix tasks to the server using its API.


## Requirements

`Pyzabbix` python 3 librarie is needed to communicate with the Zabbix server.
The librarie can be installed by running:

`pip install pyzabbix`

## How to run the code.

Simply run `python3 python_scripts/zabbix_api_communication.py` which will looks for `data.yml` in the same directory as the parameter file.

The output should be the list of monitoring task pushed to the server.



## Authors and acknowledgment
Muranovic Allan

## License
Open source project done for a Master thesis.

## Project status
The project is working, but modules can be added in `python_scripts/zabbix_api_communication.py` by modyfing the `generate_zabbix_monitoring_tasks(zabbix_credentials, zabbix_host_id, targets_ip_and_port, tasks):` function at line 327.
