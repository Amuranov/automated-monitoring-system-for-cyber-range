import yaml
import sys

from pyzabbix import ZabbixAPI, ZabbixAPIException

ZABBIX_SERVICE_PORT = 10500 # Common port used by zabbix.
HOST_NAME = "cyber_range_monitoring_host" # Name of the host created for the monitoring tasks.
LINUX_SERVER_GROUP_ID = 2					# Target to monitor are in the Linux server group
TEMPLATE_ID = 0 						# Monitoring template to use. None in our case.


def extract_data_from_yaml(f = "data.yml"):
	"""
	Parse the .yml file containing the information about the hosts, the wanted tunnels and the monitoring tasks as well.
	"""
	with open(f, "r") as file:
		data = yaml.load(file, Loader=yaml.FullLoader)
	list_hosts 				= data["hosts"]
	list_tunnels 			= data["tunnels"]
	list_monitoring_tasks 	= data["monitoring"]
	return list_hosts, list_tunnels, list_monitoring_tasks


def extract_zabbix_server_credentials(tasks):
	"""
	Parse the .yml file in order to retrieve Zabbix API credentials.
	"""
	return(tasks["zabbix_server"])


def get_zabbix_hosts_names(address, username, password):
	"""
	Return a list containing all zabbix host names.
	"""
	all_host_names = []
	try: 
		zapi = ZabbixAPI(address)
		zapi.login(username, password)
		all_hosts = zapi.host.get()
		
		for host in all_hosts:
			all_host_names.append(host["host"])
	except ZabbixAPIException as e:
		print(e)
		sys.exit()

	return(all_host_names)


def add_zabbix_host(address, username, password):
	"""
	Add host to zabbix server if does not exist yet.
	"""
	if HOST_NAME not in (get_zabbix_hosts_names(address, username, password)):
		try : 
			zapi = ZabbixAPI(address)
			zapi.login(username, password)
			test = zapi.host.get()
			test = zapi.host.create(
				host=HOST_NAME,
					interfaces={"type": 1,"main": 1,"useip": 1,"ip": "127.0.0.1","dns": "","port": ZABBIX_SERVICE_PORT},
					groups={"groupid": LINUX_SERVER_GROUP_ID,},
					template={"templateid":TEMPLATE_ID},
					inventory_mode=-1
				)
		except ZabbixAPIException as e:
			print(e)
			sys.exit()
	
def get_zabbix_host_id_from_name(zabbix_credentials):
	"""
	Return the information list = from the targeted zabbix host.
	Returns None if the host does not exist.
	""" 
	res = None
	zapi = ZabbixAPI(zabbix_credentials["api_address"])
	zapi.login(zabbix_credentials["username"], zabbix_credentials["password"])
	hosts = zapi.host.get(filter={"host": HOST_NAME}, selectInterfaces=["interfaceid"])
	if hosts:
		res = hosts
	return(res)


def get_corresponding_tunnel_port(tunnels, tunnel_name):
	"""
	Return the port of the tunnel corresponding to the given tunnel name in parameter.
	"""
	res = ""
	for tunnel in tunnels:
		if tunnel["tunnel_name"] == tunnel_name:
			res = tunnel["local_bind_address"]["port"]
	return res



def generate_list_of_targets_with_ip_and_port(hosts, tunnel_and_its_port, tasks):
	"""
	Takes the list of host, host associated with its tunnels and the list of tasks and return a list containing truple [machine_name, machine_ip, machine_port].
	The returned list contain the name of the device and where to reach it (with or without a tunnel).

	"""
	res = []
	zabbix_server_info = tasks["zabbix_server"]
	for target_name in tasks["targets"]:
		temp = []
		if "tunnel" in tasks["targets"][target_name]:
			# If the device is reachable through a tunnel
			temp.append(target_name)
			temp.append("127.0.0.1")
			temp.append(tunnel_and_its_port[tasks["targets"][target_name]["tunnel"]]) # Get its correspoding tunnel
		elif "port" in tasks["targets"][target_name]:	
			# If the device does not need a tunnel, we still need its ip and a ssh port.
			temp.append(target_name)
			temp.append(hosts[target_name]["ssh_address_or_host"])
			temp.append(tasks["targets"][target_name]["port"])
		# Adding the host ssh credentials as well
		temp.append(hosts[target_name]["ssh_username"])
		temp.append(hosts[target_name]["ssh_password"])
		res.append(temp)
	return(res)
				


def generate_tuple_containing_tunnel_name_and_port(tunnels):
	"""
	Takes the list of tunnels and return a dictionnary containing the tunnel_name as key and its port as corresponding element.
	"""
	tunnel_and_its_port = {}
	for tunnel in tunnels:
		tunnel_and_its_port[tunnel["tunnel_name"]] = tunnel["local_bind_address"]["port"]
	return(tunnel_and_its_port)


def create_machine_connection_info(targets_ip_and_port, machine_name):
	"""
	Returns  a list [machine_name, ip, port, ssh_username, ssh_password] corresponding to given machine_name
	"""
	for target in targets_ip_and_port:
		if target[0] == machine_name:
			# Returns only the target credentials, no monitoring command
			return target[:5]



def push_zabbix_monitoring_command(zabbix_credentials, task_host, info):
	"""
	Takes the zabbix server credentials, and all the needed info of a single task and push it to the server.
	"""
	for command in info[5]:		
		task_name = str(info[0] + "_command_" + command )
		key = "ssh.run[{},{},{},'UTF-8']".format(task_name, info[1], info[2])
		try:
			zapi = ZabbixAPI(zabbix_credentials["api_address"])
			zapi.login(zabbix_credentials["username"], zabbix_credentials["password"])
			item = zapi.item.create(
				hostid= task_host[0]["hostid"],
				name=task_name,
				key_=key,
				interfaceid=task_host[0]["interfaces"][0]["interfaceid"],
				authtype="0",
				username=info[3],
				password=info[4],
				params=command,
				type=13,
				value_type=4,
				delay=30
				)
		except ZabbixAPIException as e:
			print(e)
			sys.exit()
		print("Added item with itemid {0} to host: {1}".format(item["itemids"][0], task_host[0]["hostid"]))



def push_zabbix_monitoring_files_content(zabbix_credentials, task_host, info):
	"""
	Monitor the md5 hash of given files.
	"""
	for file in info[5]:	
		task_name = str(info[0] + "_files_content_" + file )
		command = str("md5sum " + str(file))
		key = "ssh.run[{},{},{},'UTF-8']".format(task_name, info[1], info[2])
		try:
			zapi = ZabbixAPI(zabbix_credentials["api_address"])
			zapi.login(zabbix_credentials["username"], zabbix_credentials["password"])
			item = zapi.item.create(
				hostid= task_host[0]["hostid"],
				name=task_name,
				key_=key,
				interfaceid=task_host[0]["interfaces"][0]["interfaceid"],
				authtype="0",
				username=info[3],
				password=info[4],
				params=command,
				type=13,
				value_type=4,
				delay=30
				)
		except ZabbixAPIException as e:
			print(e)
			sys.exit()
		print("Added item with itemid {0} to host: {1}".format(item["itemids"][0], task_host[0]["hostid"]))


def push_zabbix_monitoring_users(zabbix_credentials, task_host, info):
	"""
	Monitors the existence of given users name.

	"""
	for user in info[5]:	
		task_name = str(info[0] + "_users_" + user )
		command = str("id -u " + str(user))
		key = "ssh.run[{},{},{},'UTF-8']".format(task_name, info[1], info[2])
		try:
			zapi = ZabbixAPI(zabbix_credentials["api_address"])
			zapi.login(zabbix_credentials["username"], zabbix_credentials["password"])
			item = zapi.item.create(
				hostid= task_host[0]["hostid"],
				name=task_name,
				key_=key,
				interfaceid=task_host[0]["interfaces"][0]["interfaceid"],
				authtype="0",
				username=info[3],
				password=info[4],
				params=command,
				type=13,
				value_type=4,
				delay=30
				)
		except ZabbixAPIException as e:
			print(e)
			sys.exit()
		print("Added item with itemid {0} to host: {1}".format(item["itemids"][0], task_host[0]["hostid"]))


def push_zabbix_monitoring_version_info(zabbix_credentials, task_host, info):
	"""
	Monitor the general system informations.
	"""
	task_name = str(info[0] + "_version_info" )
	command = str("hostnamectl")
	key = "ssh.run[{},{},{},'UTF-8']".format(task_name, info[1], info[2])
	try:
		zapi = ZabbixAPI(zabbix_credentials["api_address"])
		zapi.login(zabbix_credentials["username"], zabbix_credentials["password"])
		item = zapi.item.create(
			hostid= task_host[0]["hostid"],
			name=task_name,
			key_=key,
			interfaceid=task_host[0]["interfaces"][0]["interfaceid"],
			authtype="0",
			username=info[3],
			password=info[4],
			params=command,
			type=13,
			value_type=4,
			delay=30
			)
	except ZabbixAPIException as e:
		print(e)
		sys.exit()
	print("Added item with itemid {0} to host: {1}".format(item["itemids"][0], task_host[0]["hostid"]))



def push_zabbix_monitoring_open_ports(zabbix_credentials, task_host, info):
	"""
	Monitor list of open ports.

	"""
	task_name = str(info[0] + "_open_ports" )
	command = str("lsof -i -P -n | grep LISTEN")
	key = "ssh.run[{},{},{},'UTF-8']".format(task_name, info[1], info[2])
	try:
		zapi = ZabbixAPI(zabbix_credentials["api_address"])
		zapi.login(zabbix_credentials["username"], zabbix_credentials["password"])
		item = zapi.item.create(
			hostid= task_host[0]["hostid"],
			name=task_name,
			key_=key,
			interfaceid=task_host[0]["interfaces"][0]["interfaceid"],
			authtype="0",
			username=info[3],
			password=info[4],
			params=command,
			type=13,
			value_type=4,
			delay=30
			)
	except ZabbixAPIException as e:
		print(e)
		sys.exit()
	print("Added item with itemid {0} to host: {1}".format(item["itemids"][0], task_host[0]["hostid"]))


def push_zabbix_monitoring_network_interfaces(zabbix_credentials, task_host, info):
	"""
	Monitor the network interfaces file.
	"""
	task_name = str(info[0] + "_network_interfaces" )
	command = str("cat /etc/network/interfaces")
	key = "ssh.run[{},{},{},'UTF-8']".format(task_name, info[1], info[2])
	try:
		zapi = ZabbixAPI(zabbix_credentials["api_address"])
		zapi.login(zabbix_credentials["username"], zabbix_credentials["password"])
		item = zapi.item.create(
			hostid= task_host[0]["hostid"],
			name=task_name,
			key_=key,
			interfaceid=task_host[0]["interfaces"][0]["interfaceid"],
			authtype="0",
			username=info[3],
			password=info[4],
			params=command,
			type=13,
			value_type=4,
			delay=30
			)
	except ZabbixAPIException as e:
		print(e)
		sys.exit()
	print("Added item with itemid {0} to host: {1}".format(item["itemids"][0], task_host[0]["hostid"]))




def generate_zabbix_monitoring_tasks(zabbix_credentials, zabbix_host_id, targets_ip_and_port, tasks):
	"""
	This function take a list contianing all devices name, ip to reach it and port, and the monitoring tasks as parametre.
	Return a list containing all needed elements to push to zabbix api.
	"""
	# Always checking if the host is configured, if not, it adds it.
	for task in tasks["targets"]:
		for module in tasks["targets"][task]:
			machine_info = create_machine_connection_info(targets_ip_and_port, task)

			if module == "command":
			# Basic "command" module chosen
				machine_info.append(tasks["targets"][task]["command"])
				push_zabbix_monitoring_command(zabbix_credentials, zabbix_host_id, machine_info)


			elif module == "files_content":
				# Files_content module chosen
				machine_info.append(tasks["targets"][task]["files_content"])
				push_zabbix_monitoring_files_content(zabbix_credentials, zabbix_host_id, machine_info)

			elif module == "users":
				# Display list of users
				machine_info.append(tasks["targets"][task]["users"])
				push_zabbix_monitoring_users(zabbix_credentials, zabbix_host_id, machine_info)

			elif module == "version_info":
				# Display systeme info
				machine_info.append(tasks["targets"][task]["version_info"])
				push_zabbix_monitoring_version_info(zabbix_credentials, zabbix_host_id, machine_info)

			elif module == "open_ports":
				# Display open ports
				machine_info.append(tasks["targets"][task]["open_ports"])
				push_zabbix_monitoring_open_ports(zabbix_credentials, zabbix_host_id, machine_info)

			elif module == "network_interfaces":
				# Display the network interfaces files
				machine_info.append(tasks["targets"][task]["network_interfaces"])
				push_zabbix_monitoring_network_interfaces(zabbix_credentials, zabbix_host_id, machine_info)



if __name__ == '__main__':
	hosts, tunnels, tasks = extract_data_from_yaml()
	zabbix_credentials    = extract_zabbix_server_credentials(tasks)

	add_zabbix_host(zabbix_credentials["api_address"], zabbix_credentials["username"], zabbix_credentials["password"]) # Create the host on which we add the tasks

	zabbix_host_id        = get_zabbix_host_id_from_name(zabbix_credentials) # retrieve the host id
	tunnel_and_its_port   = generate_tuple_containing_tunnel_name_and_port(tunnels)  
	# Generate the list containing the target, which ip and port to join them (depending if we connect to them through a tunnel or not)
	targets_ip_and_ports  = generate_list_of_targets_with_ip_and_port( hosts, tunnel_and_its_port, tasks)


	generate_zabbix_monitoring_tasks(zabbix_credentials, zabbix_host_id, targets_ip_and_ports, tasks)
	# Pushing each monitoring task to zabbix server