from pytunneling import TunnelNetwork
from time import sleep
import sys
import json
import argparse
import yaml


TUNNEL_PATH = "127.0.0.1" # the tunnel begins on the machine executing the script.



def parseArguments():
	""" 
	User can chose by providing a json file or directly give the information by argument.
	Target port of last machine is configurable as well.
	"""
	parser = argparse.ArgumentParser()
	# Optional arguments
	parser.add_argument("-f", "--file", help="Json file to read.", type=str, default=None)
	parser.add_argument("-j", "--jumps", help="Directly provided machines to jump through( ip, user, password  (ip2, user2, password2))...", type=str, default=None)
	parser.add_argument("-p", "--port", help="SSH port of the last machine (by default: 22).", type=int, default=22)

	# Parse arguments
	args = parser.parse_args()
	return args


def write_path_to_log(path, local_port):
	""" 
	Writing the script output to a log file.
	"""
	with open("log.txt", "w") as f:
		line = str("\nTunnel open at " + str(path) + ":" + str(local_port) + "\n")
		f.write(line)


def tunnel_creation(counter, info, port):
	"""
	Takes the information (ip of the target, username, password) and creates the ssh tunnel.
	"""
	while counter < len(info):
		with TunnelNetwork(tunnel_info=info[counter], target_ip="127.0.0.1", target_port=port) as tn:
			write_path_to_log(TUNNEL_PATH, tn.local_bind_port)
			print("Tunnel with path : {0} available at 127.0.0.1:{1}".format(TUNNEL_PATH, tn.local_bind_port))
			counter = counter + 1
			tunnel_creation(counter, info, port)
			while True:
				# Use this tunnel
				sleep(5)


def extract_data_from_yaml(f = "data.yml"):
	"""
	Parse the .yml file containing the information about the hosts, the wanted tunnels and the monitoring tasks as well.
	"""
	with open(f, "r") as file:
		data = yaml.load(file, Loader=yaml.FullLoader)
	list_hosts 				= data["hosts"]
	list_tunnels 			= data["tunnels"]
	list_monitoring_tasks 	= data["monitoring"]

	return list_hosts, list_tunnels, list_monitoring_tasks


def create_tunnel_info(hosts, tunnels):
	"""
	This functions goes through the "tunnel" and "hosts" section of the .yml file to merge them and create the tunnels.

	"""
	all_tunnel = []
	counter = 0
	for tunnel in tunnels:
		path = []
		for jump in tunnel["path"]:
			temp_dic = {}
			temp_dic["ssh_address_or_host"] = hosts[jump]["ssh_address_or_host"]
			temp_dic["ssh_username"] = hosts[jump]["ssh_username"]
			temp_dic["ssh_password"] = hosts[jump]["ssh_password"]
			path.append(temp_dic)
		# Adding the chosen local machine port on which to bind the tunnel
		path[-1]["local_bind_address"] = (tunnel["local_bind_address"]["address"], tunnel["local_bind_address"]["port"])
		all_tunnel.append(path)
	return all_tunnel




if __name__ == '__main__':
	
	args = parseArguments()	
	for a in args.__dict__:
		# If information is in a .yml file
		if (a == "file") and (args.__dict__[a] == None):
			hosts, tunnels, tasks = extract_data_from_yaml()
			tunnel_info = create_tunnel_info(hosts, tunnels)
			tunnel_creation(0, tunnel_info, 22)
		elif (a == "file") and (args.__dict__[a] != None):
			hosts, tunnels, tasks = extract_data_from_yaml(args.__dict__[a])
			tunnel_info = create_tunnel_info(hosts, tunnels)
			tunnel_creation(0, tunnel_info, 22)
